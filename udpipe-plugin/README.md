# Plugins for blacklab autosearch to convert text into Conllu or annotated TEI

This library contains two plugins to convert plain text. One to convert into Conllu and one to convert into TEI-Xml with linguistic annotations. The conversions use two generic processes. One that sends text to a udpipe service that returns plain text in conllu format, see https://central.sonatype.com/artifact/org.fryske-akademy/udpipe-service and one that uses xslt to convert conllu into customized TEI-xml, see https://central.sonatype.com/artifact/org.fryske-akademy/TeiLinguisticsFa.

## Dependencies apart from blacklab
```xml
    <dependency>
      <groupId>com.vectorprint</groupId>
      <artifactId>VectorPrintCommon</artifactId>
      <version>8.2</version>
    </dependency>
<!-- this dependency is only needed for conversion to TEI -->
    <dependency>
      <groupId>org.fryske-akademy</groupId>
      <artifactId>TeiLinguisticsFa</artifactId>
      <version>8.0</version>
    </dependency>
<!-- saxon is probably available in blacklab-->
    <dependency>
      <groupId>net.sf.saxon</groupId>
      <artifactId>Saxon-HE</artifactId>
      <version>12.4</version>
      <scope>provided</scope>
    </dependency>

```
`slf4j api` is used for logging

## Plugin configuration
See http://inl.github.io/BlackLab/development/customization/plugins.html

- plugin ids are `TextToTEI` and `TextToConllu`
- NOTE: you need to include `tagPlugin: "noop"` to make the converters work
- a required `udpipeUrl=<url to udpipe REST service>`
- an optional `timeoutSeconds=60`
- example
```yaml
plugins:
  plugins:
    TextToTEI:
      udpipeUrl: http://somehost:someport/udpipeservice/udpipe/process/conllu
```

## Usage
- put this jar and it's dependencies on blacklab's classpath
    - see documentation of your servlet-container
        - easiest is to put all dependencies of your plugin in `WEB-INF/lib`
- follow https://inl.github.io/BlackLab/development/customization/plugins.html
    - exclude the jarPath bit
- example:
```yaml
convertPlugin: TextToTEI
tagPlugin: noop
```