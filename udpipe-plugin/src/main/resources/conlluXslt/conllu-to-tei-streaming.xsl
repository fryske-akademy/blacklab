<xsl:transform xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0">

  <xsl:param name="conllu"/>
  <xsl:param name="template" select="'tei-template.xml'"/>
  <xsl:variable name="tei" select="doc($template)"/>

  <xsl:include href="process-template.xsl"/>

  <xsl:template match="/">
      <xsl:apply-templates select="$tei/*">
        <xsl:with-param name="lines" select="unparsed-text-lines($conllu)"/>
      </xsl:apply-templates>
  </xsl:template>
</xsl:transform>
