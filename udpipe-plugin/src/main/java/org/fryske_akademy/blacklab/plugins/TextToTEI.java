package org.fryske_akademy.blacklab.plugins;

import nl.inl.blacklab.exceptions.PluginException;
import org.fa.tei.xslt.XslTransformer;

import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.Map;

public class TextToTEI extends TextToConllu {

    public static final String OUTPUT_TYPE = "application/tei+xml";
    public static final String CONLLU_TO_TEI_STREAMING_DIR = System.getProperty("java.io.tmpdir")+"/conllu-to-tei-streaming";
    public static final String CONLLU_XSLT_DIR = "conlluXslt";
    public static final String NO_CONLLU_AVAILABLE = "No conllu available";

    @Override
    public String getOutputFormat() {
        return OUTPUT_TYPE;
    }


    @Override
    public void perform(InputStream inputStream, Charset charset, String s, OutputStream outputStream) throws PluginException {
        try {
            final File tempFile = File.createTempFile("conllu", null);
            tempFile.deleteOnExit();
            try (FileOutputStream fos = new FileOutputStream(tempFile)) {
                super.perform(inputStream, charset, s, fos);
            }
            if (tempFile.length() == 0) {
                throw new PluginException(NO_CONLLU_AVAILABLE);
            }
            conlluToTei(outputStream, tempFile);
            tempFile.delete();
        } catch (TransformerException | IOException e) {
            throw new PluginException(e);
        }
    }

    public void conlluToTei(OutputStream outputStream, File file) throws MalformedURLException, TransformerException {
        XslTransformer xslTransformer = new XslTransformer(CONLLU_TO_TEI_STREAMING_DIR+"/conllu-to-tei-streaming.xsl");
        xslTransformer.addParameter("conllu", String.valueOf(file.toURI().toURL()));
        xslTransformer.streamTransform(new StringReader("<dummy/>"), new OutputStreamWriter(outputStream));
    }

    @Override
    public String getId() {
        return "TextToTEI";
    }

    @Override
    public String getDisplayName() {
        return "text to TEI";
    }

    @Override
    public String getDescription() {
        return "Plugin that will convert plain text to TEI via conllu using udpipe and xslt";
    }

    @Override
    public void init(Map<String, String> map) throws PluginException {
        super.init(map);
        // make xslt and template available
        new File(CONLLU_TO_TEI_STREAMING_DIR).mkdir();

        final URL location = TextToTEI.class.getProtectionDomain().getCodeSource().getLocation();

        try {
            if (location.getPath().endsWith(".jar")) {
                extract(Path.of(location.getPath()));
            } else {
                // TODO this branch is for junit only
                // TODO make xslt resources available via maven central instead of cpying from taaldatabanken project
                Path xslt = Path.of(location.getPath()).resolve(CONLLU_XSLT_DIR);
                Files.list(xslt).forEach(path -> {
                    Path copy = Path.of(CONLLU_TO_TEI_STREAMING_DIR+"/"+path.getFileName());
                    try {
                        Files.copy(path, copy, StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
        } catch (IOException e) {
            throw new PluginException(e);
        }
    }

    private void extract(Path zip) throws IOException {
        try (FileSystem fs = FileSystems.newFileSystem(zip, Collections.emptyMap())) {
            fs.getRootDirectories()
                    .forEach(root -> {
                        // in a full implementation, you'd have to
                        // handle directories
                        try {
                            Files.walk(root)
                                    .filter(p -> p.getParent() != null &&
                                            p.getFileName().toString().contains(".") &&
                                            p.getParent().getFileName() != null &&
                                            CONLLU_XSLT_DIR.equals(p.getParent().getFileName().toString()))
                                    .forEach(path -> {
                                        try {
                                            Files.copy(path, Path.of(CONLLU_TO_TEI_STREAMING_DIR+"/"+path.getFileName()), StandardCopyOption.REPLACE_EXISTING);
                                        } catch (IOException e) {
                                            throw new RuntimeException(e);
                                        }
                                    });
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }
    }

}
