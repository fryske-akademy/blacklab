package org.fryske_akademy.blacklab.plugins;

import com.vectorprint.RequestHelper;
import nl.inl.blacklab.exceptions.PluginException;
import nl.inl.blacklab.indexers.preprocess.ConvertPlugin;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.URI;
import java.net.http.HttpRequest;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class TextToConllu implements ConvertPlugin {

    public static final String FILE_EXTENSION = "txt";
    public static final String OUTPUT_TYPE = "text/conllu+plain";
    public static final String UDPIPE_URL_PROPERTY = "udpipeUrl";
    public static final String TIMEOUT_SECONDS_PROPERTY = "timeoutSeconds";
    private String udpipeUrl;
    private static final RequestHelper requestHelper = new RequestHelper();
    private int timeoutSeconds = 60;

    @Override
    public Set<String> getInputFormats() {
        return Set.of(FILE_EXTENSION);
    }

    @Override
    public String getOutputFormat() {
        return OUTPUT_TYPE;
    }

    @Override
    public boolean canConvert(PushbackInputStream pushbackInputStream, Charset charset, String s) {
        return FILE_EXTENSION.equals(s);
    }

    protected HttpRequest.Builder headers(HttpRequest.Builder builder) {
        return builder.setHeader("User-Agent", "java.net.http.HttpClient")
                .header("Content-Type", "text/plain");
    }

    private HttpRequest udpipeRequest(InputStream text) {
        return headers(HttpRequest.newBuilder()
                .uri(URI.create(udpipeUrl))
                .POST(HttpRequest.BodyPublishers.ofInputStream(() -> text))).build();
    }

    @Override
    public void perform(InputStream inputStream, Charset charset, String s, OutputStream outputStream) throws PluginException {
        if (!StandardCharsets.UTF_8.equals(charset)) {
            throw new PluginException(String.format("Unsupported charset: %s", charset));
        }
        try  {
            requestHelper.request(timeoutSeconds, udpipeRequest(inputStream), outputStream);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            throw new PluginException(e);
        }
    }

    @Override
    public String getId() {
        return "TextToConllu";
    }

    @Override
    public String getDisplayName() {
        return "Text to Conllu";
    }

    @Override
    public String getDescription() {
        return "Plugin that will convert plain text to conllu using udpipe";
    }

    @Override
    public void init(Map<String, String> map) throws PluginException {
        if (!map.containsKey(UDPIPE_URL_PROPERTY)) {
            throw new PluginException("udpipeUrl not present");
        }
        udpipeUrl = map.get(UDPIPE_URL_PROPERTY);
        if (map.containsKey(TIMEOUT_SECONDS_PROPERTY)) {
            try {
                timeoutSeconds = Integer.parseInt(map.get(TIMEOUT_SECONDS_PROPERTY));
            } catch (NumberFormatException e) {
                throw new PluginException("timeout seconds should be an integer");
            }
        }
    }

    @Override
    public boolean needsConfig() {
        return true;
    }
}
