package org.fryske_akademy.blacklab.plugins;

import nl.inl.blacklab.exceptions.PluginException;
import org.fa.tei.validation.ValidationHelper;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class FrisianTextToTEITest {

    @Test
    public void fryskInputHandlerTest() throws PluginException, IOException, SAXException, URISyntaxException {
        TextToTEI frisianTextToTEI = new TextToTEI();
        final InputStream in = FrisianTextToTEITest.class.getResourceAsStream("/frysk.txt");
        OutputStream out = new ByteArrayOutputStream();
        frisianTextToTEI.init(Map.of(TextToTEI.UDPIPE_URL_PROPERTY,"https://frisian.eu/udpipeservice/udpipe/process/conllu"));
        try {
            frisianTextToTEI.perform(in, StandardCharsets.UTF_8,"txt",out);
            ValidationHelper.validateXml(out.toString());
        } catch (PluginException e) {
            if (TextToTEI.NO_CONLLU_AVAILABLE.equals(e.getMessage())) {
                System.err.println("\nThis integration test needs to reach an external udpipe server!!");
            } else {
                throw e;
            }
        }
    }

    @Test
    public void testConlluToTEI() throws PluginException, IOException, SAXException, TransformerException {
        TextToTEI frisianTextToTEI = new TextToTEI();
        frisianTextToTEI.init(Map.of(TextToTEI.UDPIPE_URL_PROPERTY,"https://frisian.eu/udpipeservice/udpipe/process/conllu"));
        final File in = new File("src/test/resources/test.conllu");
        OutputStream out = new ByteArrayOutputStream();
        frisianTextToTEI.conlluToTei(out,in);
        ValidationHelper.validateXml(out.toString());
    }
}
