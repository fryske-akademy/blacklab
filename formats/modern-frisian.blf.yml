# GENERATED (xsltproc OddToBlacklabProperties.xsl ../corpora_linguistics.odd > modern-frisian.blf.yml), ADAPT AS NEEDED
namespaces:

  tei: "http://www.tei-c.org/ns/1.0"
  fa: "http://frisian.eu/tei-ud-linguistics"

fileTypeOptions:
  processing: saxon

documentPath: /tei:TEI

annotatedFields:

  contents:

    containerPath: tei:text

    wordPath: .//tei:w

    # capture word ids so we can refer to them from the standoff annotations
    tokenIdPath: "@xml:id"

    punctPath: ".//text()[.!='' and preceding-sibling::tei:w]|.//tei:pc |.//tei:lb"

    annotations:

      - name: word
        description: word forms
        valuePath: "string-join(.//text(),'')"
        sensitivity: si

      - name: lemma
        description: lemma of word forms
        valuePath: "@lemma"
        sensitivity: si

      - name: compound
        description: lemma of split forms
        valuePath: "let $xid := @xml:id, $l := if ($xid) then ../tei:linkGrp/tei:link[@lemma and matches(@target,'^#'||$xid||' ')]/@lemma else () return if (count($l)>1) then string-join(('ERR: ',$l),', ') else $l"
        sensitivity: si


      - name: islemma
        valuePath: "@fa:islemma or tei:m/@fa:islemma"
        description: "Boolean, is this a base form"
        uiType: select

      - name: abbr
        valuePath: "@fa:abbr or tei:m/@fa:abbr"
        description: "Boolean feature. Is this an abbreviation?"
        uiType: select

      - name: poss
        valuePath: "@fa:poss or tei:m/@fa:poss"
        description: "Boolean feature. Is this word possessive?"
        uiType: select

      - name: reflex
        valuePath: "@fa:reflex or tei:m/@fa:reflex"
        description: "Boolean feature, typically of pronouns or determiners. It tells whether the word is reflexive, i.e. refers to the subject of its clause.?"
        uiType: select

      - name: prefix
        valuePath: "@fa:prefix or tei:m/@fa:prefix"
        description: "Boolean feature, Is this a prefix word in a compound, that usually cannot stand on its own?"
        uiType: select

      - name: prontype
        valuePath: "@fa:prontype or tei:m/@fa:prontype"
        description: "This feature typically applies to pronouns, pronominal adjectives (determiners), pronominal numerals (quantifiers) and pronominal adverbs."
        uiType: select

      - name: case
        valuePath: "@fa:case or tei:m/@fa:case"
        description: "Case is usually an inflectional feature of nouns."
        uiType: select

      - name: tense
        valuePath: "@fa:tense or tei:m/@fa:tense"
        description: "Tense is typically a feature of verbs."
        uiType: select

      - name: voice
        valuePath: "@fa:voice or tei:m/@fa:voice"
        description: "Voice is typically a feature of verbs."
        uiType: select

      - name: number
        valuePath: "@fa:number or tei:m/@fa:number"
        description: "Number is usually an inflectional feature of nouns."
        uiType: select

      - name: person
        valuePath: "@fa:person or tei:m/@fa:person"
        description: "Person is typically feature of personal and possessive pronouns / determiners, and of verbs."
        uiType: select

      - name: verbtype
        valuePath: "@fa:verbtype or tei:m/@fa:verbtype"
        description: "distinctions on top of verb and aux."
        uiType: select

      - name: verbform
        valuePath: "@fa:verbform or tei:m/@fa:verbform"
        description: "form of verb or deverbative."
        uiType: select

      - name: polite
        valuePath: "@fa:polite or tei:m/@fa:polite"
        description: "Various languages have various means to express politeness or respect."
        uiType: select

      - name: numtype
        valuePath: "@fa:numtype or tei:m/@fa:numtype"
        description: "numeral type."
        uiType: select

      - name: degree
        valuePath: "@fa:degree or tei:m/@fa:degree"
        description: "Degree of comparison is typically an inflectional feature of some adjectives and adverbs."
        uiType: select

      - name: mood
        valuePath: "@fa:mood or tei:m/@fa:mood"
        description: "Mood is a feature that expresses modality and subclassifies finite verb forms."
        uiType: select

      - name: gender
        valuePath: "@fa:gender or tei:m/@fa:gender"
        description: "gender."
        uiType: select

      - name: hyph
        valuePath: "@fa:hyph or tei:m/@fa:hyph"
        description: "Is this part of a hyphenated compound? Depending on tokenization, the compound may be one token or be split to several tokens; then the tokens need tags."
        uiType: select

      - name: prodrop
        valuePath: "@fa:prodrop or tei:m/@fa:prodrop"
        description: "Added for Frisian to MISC in universaldependencies. pronoun drop, omission of pronouns because they can be inferred"
        uiType: select

      - name: clitic
        valuePath: "@fa:clitic or tei:m/@fa:clitic"
        description: "Added for Frisian to features in universaldependencies. Most personal pronouns have a clitic form, which is the result of either vowel deletion, vowel reduction, monophthongization or schwa deletion, while there are also cases of suppletion."
        uiType: select

      - name: compound
        valuePath: "@fa:compound or tei:m/@fa:compound"
        description: "Added for Frisian to features in universaldependencies. The univerbation of two or more words."
        uiType: select

      - name: inflection
        valuePath: "@fa:inflection or tei:m/@fa:inflection"
        description: "Not in universaldependencies. The modification of a word to express different grammatical categories such as tense, case, voice, aspect, person."
        uiType: select

      - name: suffix
        valuePath: "@fa:suffix or tei:m/@fa:suffix"
        description: "Not in universaldependencies Boolean feature, Is this a suffix word in a compound, that usually cannot stand on its own?"
        uiType: select

      - name: valency
        valuePath: "@fa:valency or tei:m/@fa:valency"
        description: "Not in universaldependencies. Verb valency or valence is the number of arguments controlled by a verbal predicate."
        uiType: select

      - name: convertedfrom
        valuePath: "@fa:convertedfrom or tei:m/@fa:convertedfrom"
        description: "Not in universaldependencies. Words belonging to one part of speech category used as another category."
        uiType: select

      - name: predicate
        valuePath: "@fa:predicate or tei:m/@fa:predicate"
        description: "Not in universaldependencies. Predicate."
        uiType: select

      - name: construction
        valuePath: "@fa:construction or tei:m/@fa:construction"
        description: "Not in universaldependencies. Construction."
        uiType: select

      - name: pos
        valuePath: "@pos or tei:m/@pos"
        description: "These tags mark the core part-of-speech categories."
        uiType: select


    inlineTags:
      # Sentence tags
      - path: .//tei:s

    standoffAnnotations:
      - path: ".//tei:linkGrp[@targFunc='head argument']/tei:link"
        type: relation
        valuePath: "./@ana"
        # Note that we make sure the root relation is indexed without a source,
        # which is required in BlackLab.
        sourcePath: "if (./@ana = 'root') then '' else replace(./@target, '^#(.+) .+$', '$1')"
        targetPath: "replace(./@target, '^.+ #(.+)$', '$1')"

metadata:

  containerPath: tei:teiHeader

  fields:

    - name: title
      valuePath: "tei:fileDesc/tei:titleStmt/tei:title"

    - name: author
      valuePath: "concat(.//tei:persName[1]/tei:forename, ' ',.//tei:persName[1]/tei:surname, normalize-space(.//tei:persName[1]/text()), ' (db key=', .//tei:persName[1]/tei:ref/@target,')')"

    - name: authorplace
      valuePath: ".//tei:person[1]/tei:residence/tei:placeName"
      displayName: Author residence

    - name: authorcode
      valuePath: ".//tei:person[1]/tei:idno"
      displayName: Author code

    - name: year
      valuePath: ".//tei:date[parent::tei:bibl or parent::tei:publicationStmt][1]/text()"
      uiType: range

    - name: source
      valuePath: ".//tei:sourceDesc/tei:msDesc/tei:head/text()"

    - name: remarks
      valuePath: ".//tei:notesStmt/tei:note/text()"

    - name: state
      valuePath: "concat('tokenized: ', boolean(//tei:w[1]), ', lemmatized ', boolean(//tei:w[@lemma][1]), ', pos tagged: ', boolean(//tei:w[@pos][1]) )"
      displayName: Document state

    - name: "language_variant"
      valuePath: "tei:profileDesc/tei:langUsage/tei:language/text()"
      displayName: Language Variant

    # remove this for internal use where all material may be viewed, then generate new index!

    - name: contentViewable
      valuePath: .//tei:availability[1]/@status='free'
      displayName: free material

corpusConfig:

  displayName: Frisian corpora

  description: annotated frisian texts, detail level of annotation varies, midfrysk is most complete and detailed

  # optIn, set this to true for internal use

  contentViewable: false

  specialFields:
    dateField: year

  annotationGroups:
    contents:
      - name: Generic aspects
        annotations:
          - word
          - lemma
          - compound
          - pos
      - name: Verb linguistics
        annotations:
          - tense
          - person
          - verbform
          - prodrop
          - clitic
          - mood
          - aux
          - voice
          - valency
      - name: Noun linguistics
        annotations:
          - case
          - number
          - gender
          - degree
          - diminutive
      - name: Other linguistics
        addRemainingAnnotations: true

  metadataFieldGroups:
    - name: Main filters
      fields:
        - year
        - author
        - language_variant
        - state
    - name: Other filters
      addRemainingFields: true


