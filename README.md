# blacklab

Strategy and setup for deployment of customized https://github.com/INL/BlackLab and https://github.com/INL/corpus-frontend.

This repo assumes war builds from https://github.com/INL/BlackLab/tree/experiment/fa-tomcat-10 and https://github.com/fryske-akademy/corpus-frontend/tree/jakarta for now.

## customization (normally not needed)

### fork

clone the versions of Blacklab and corpus-front-end mentioned above 

## installation

Index, images and config will be docker (path) volumes, see [docker](docker/README.md)

### index (see also [multiple-indexes.md](multiple-indexes.md))

- if needed pull in latest generated blacklab config from https://bitbucket.org/fryske-akademy/tei-encoding/src/master/src/main/resources/facustomization/blacklab
- if an index is desired that does not restrict access to content, first change "contentViewable" in fa-tei.blf.yml
- copy formats/fa-tei.blf.yml in /etc/blacklab/formats
  - build backlab index "frysk" for "fa-tei" format (see http://inl.github.io/BlackLab/guide/)
    - create an empty directory blacklabindexes/frysk
    - in case of problems (indexing seems to hang) restart with empty directory or lower number of threads or memory
    - ```java -cp blacklab-tools/blacklab-xxx.jar:blacklab-tools/lib nl.inl.blacklab.tools.IndexTool --threads 8 create blacklabindexes/frysk "tdb1/*xml" fa-tei```
    - to add more: ```java -cp blacklab-tools/blacklab-xxx.jar:blacklab-tools/lib nl.inl.blacklab.tools.IndexTool --threads 8 add blacklabindexes/frysk "teidir/*xml" fa-tei```
    - to update first delete using lucene query, i.e.: `java -cp blacklab-tools/blacklab-xxx.jar:blacklab-tools/lib nl.inl.blacklab.tools.IndexTool delete blacklabindexes/frysk/ "language_variant:Nije stavering of Steatestavering (1980 oant no)"`
- copy index to /`<volumeroot>`/blacklab/blacklabindexes on target machine

### images

- copy images to /`<volumeroot>`/blacklab/blacklabimages on target machine

### frontend

The frontend needs several libraries that will be bundled in a module using webpack.

- `cd frysk/static/webpack`
- `npm install`

### config

- archive and copy config files in this folder
    - ```tar cvf blacklabconfig.tar blacklab-server.json corpus-frontend.properties formats/ frysk/```
    - ```mkdir -p /<volumeroot>/blacklab/blacklab``` on target machine
    - ```cd /<volumeroot>/blacklab/blacklab``` on target machine
    - ```tar xvf blacklabconfig.tar```
 
### permissions

- groupadd payara
- useradd payara -g payara
- chown -R payara:payara /<volumeroot>/blacklab/
- chmod -R o+rx /<volumeroot>/blacklab/blacklabimages

### build and run

see [docker](docker/README.md)