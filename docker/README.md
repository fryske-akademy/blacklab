# docker setup for blacklab

## in the build directory

### create file 'changepwd' to change master and admin password:
```
AS_ADMIN_PASSWORD=admin
AS_ADMIN_NEWPASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=changeit
AS_ADMIN_NEWMASTERPASSWORD=xxxxxxx
```
### create file 'pwdprd' to use new passwords and for dbcon:
```
AS_ADMIN_PASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=xxxxxxx
AS_ADMIN_ALIASPASSWORD=xxxxxxx
```
### DON'T COMMIT THE PASSWORD FILES AND REMOVE AFTER USE!!!


### copy wars and prepare deploy.txt in wars directory

- download blacklab server war from maven (nl.inl.blacklab)
- download accompanying corpus-frontend from https://github.com/INL/corpus-frontend/tags

### environment
```bash
export APPNAME=blacklab
export VOLUMEROOT=${HOME}/volumes
export VERSION=xxx
```
### cantaloupe
**RUN a proxy for /corpusiiif/2!**  
For apache, this is enough:
```
        AllowEncodedSlashes NoDecode
        ProxyPass "/corpusiiif/2/" "http://host:8183/iiif/2/" nocanon
```
### build images:
```
export DOCKER_BUILDKIT=1
docker build --secret id=changepwd,src=changepwd --secret id=pwdprd,src=pwdprd --secret id=payarainit,src=payarainit -t ${APPNAME}:${VERSION} .
rm changepwd pwdprd
```
## optionally push to registry
```
docker tag ${APPNAME}:${VERSION} localhost:5000/${APPNAME}:${VERSION}
docker run -d -p 5000:5000 --restart=always --name registry registry:2
docker push localhost:5000/${APPNAME}
```

## in the stack directory

### prepare and run stack
**NOTE**: create the secrets in /bin/sh, without history  
**NOTE**: look carefully at volumes you may not want to overwrite
```
sh
docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
echo "AS_ADMIN_MASTERPASSWORD=xxxxxxx"|docker secret create master -
```
Prepare the volumes in ${VOLUMEROOT}/${APPNAME}, see comment in compose file, **PERMISSIONS!**
, see also [../README.md](../README.md)
```
docker stack deploy -c docker-compose.yml ${APPNAME}
```

### usefull commands

```
docker container|service|image|stack ls
docker service logs ${APPNAME}
docker stack rm ${APPNAME}
docker exec -it <container> "/bin/bash"
```

### after first stack deploy

- read and apply comment in docker-compose.yml
