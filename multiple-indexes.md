# how to setup blacklab and corpus-frontend with multiple indexes and different functionality

see also https://github.com/INL/corpus-frontend#configuration and https://inl.github.io/BlackLab/server/configuration.html

Basically what you do is (see https://bitbucket.org/fryske-akademy/oorkonden):

- depending on docker setup and proxy setup make sure blacklab-server and corpus-frontend can find their config
- configure indexing for each index in separate *.blf.yml files
- create multiple indexes in separate directories each from their own *.blf.yml
  - the index command for format `aaa` looks like this
    - `java -cp blacklab-tools/blacklab-xxx.jar:blacklab-tools/lib nl.inl.blacklab.tools.IndexTool --format-dir dirwith*blf.yml --threads 8 create indexdir/aaa "aaasourcedir/*xml" aaa`
- create a directory "default" for corpus-frontend, holding default files
- create separate directories for non default files for corpus-frontend


# directory layout for running
```
./blacklab => normally the place for blacklab-server.json and corpus-frontend.properties
./blacklab/default => corpus-frontend fallback location to look for files
./blacklab/default/Stylesheets-7.56.0 => tei stylesheets
./blacklab/default/static => js, css, html etc.
./blacklab/default/static/locales => nl-nl.json etc.
./blacklab/default/static/images
./blacklabindexes/Frisian-Charters => a blacklab lucene index
./blacklab/Frisian-Charters => location for non default corpus-frontend files
./blacklabindexes/Modern-Frisian => another index
./blacklab/Modern-Frisian => location for non default corpus-frontend files
./blacklab/Modern-Frisian/static
./blacklab/Modern-Frisian/static/webpack
```
# tips
- use the same name for the format, the index directory and the directory for frontend customization
- the developer console and network analyzer in your browser helps in locating problems
  - especially path problems due to wrong configuration and javascript errors are easy to be found this way
- file references in non default xslt may need "../default/..."
- avoid using symlinks, try to adapt your configuration first, symlinks may hide config problems