var scrollUp = function (selector) {
    var c = document.querySelector(selector);
    window.setTimeout(function () {
        $(c).animate({scrollTop: 0}, 500);
    }, 600);
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
$(document).ready(function () {
    if (window.vuexModules.corpus) {
        var x = true;
        var ui = vuexModules.ui.actions;
        ui.helpers.configureAnnotations([
            [               ,    'EXTENDED'    ,    'ADVANCED'    ,    'EXPLORE'    ,    'SORT'    ,    'GROUP'    ,    'RESULTS'    ,    'CONCORDANCE'    ],

            // Generic aspects
            ['word'         ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['lemma'        ,        x         ,        x         ,        x        ,      x       ,       x       ,        x        ,                     ],
            ['compound'     ,        x         ,        x         ,        x        ,      x       ,       x       ,        x        ,                     ],
            ['pos'          ,        x         ,        x         ,        x        ,      x       ,       x       ,        x        ,                     ],

            // Verb linguistics
            ['tense'        ,        x         ,        x         ,        x        ,      x       ,       x       ,        x        ,                     ],
            ['person'       ,        x         ,        x         ,        x        ,      x       ,       x       ,        x        ,                     ],
            ['verbform'     ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['mood'         ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['voice'        ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['valency'      ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],

            // Noun linguistics
            ['case'         ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['number'       ,        x         ,        x         ,        x        ,      x       ,       x       ,        x        ,                     ],
            ['gender'       ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['degree'       ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['diminutive'   ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],

            // Other linguistics
            ['islemma'      ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['abbr'         ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['poss'         ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['reflex'       ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['prefix'       ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['suffix'       ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['prontype'     ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['verbtype'     ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['polite'       ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['numtype'      ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['pronoun'      ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['inflection'   ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['construction' ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['convertedfrom',        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['predicate'    ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],

            // (not in any group)
            ['punct'        ,                  ,                  ,                 ,              ,               ,                 ,                     ],
            ['starttag'     ,                  ,                  ,                 ,              ,               ,                 ,                     ],
        ]);

        ui.helpers.configureMetadata([
            [                  ,    'FILTER'    ,    'SORT'    ,    'GROUP'    ,    'RESULTS/HITS'    ,    'RESULTS/DOCS'    ,    'EXPORT'    ],


            // Other filters
            ['authorcode'      ,       x        ,      x       ,       x       ,                      ,                      ,                ],
            ['authorplace'     ,       x        ,      x       ,       x       ,          x           ,          x           ,       x        ],
            ['contentViewable' ,       x        ,      x       ,       x       ,                      ,                      ,                ],
            ['fromInputFile'   ,       x        ,      x       ,       x       ,                      ,                      ,                ],
            ['remarks'         ,       x        ,      x       ,       x       ,                      ,                      ,                ],
            ['source'          ,       x        ,      x       ,       x       ,                      ,                      ,                ],
            ['title'           ,       x        ,      x       ,       x       ,                      ,                      ,                ],
            // Main filters
            ['year'            ,       x        ,      x       ,       x       ,          x           ,          x           ,       x        ],
            ['author'          ,       x        ,      x       ,       x       ,          x           ,          x           ,       x        ],
            ['language_variant',       x        ,      x       ,       x       ,                      ,                      ,                ],
            ['state'           ,       x        ,      x       ,       x       ,                      ,                      ,                ],
        ]);
    }
    if (typeof vuexModules.root.actions.distributionAnnotation === "function") {
       vuexModules.root.actions.distributionAnnotation({
           displayName: 'Token/Part of Speech Distribution',
           id: 'pos'
       });
    }
});
const unsubscribe = vuexModules.root.store.watch(state => state.corpus, () => {
    if (vuexModules.corpus) {
        var annos = vuexModules.corpus.get.allAnnotations();
        for (a in annos) {
            var ann = annos[a];
            if (ann.values) {
                for (v in ann.values) {
                    var val = ann.values[v];
                    if (val.value) {
                        val.title = FALINGUISTICS.getDocumentation(ann.id + "." + val.value);
                    }
                }
            }
        }
        unsubscribe(); // anders wordt hij misschien meerdere keren aangeroepen
    }
});
