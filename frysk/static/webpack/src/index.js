import { request, gql } from 'graphql-request'
import {print} from "graphql";

export function scrollUp(selector) {
    var c = document.querySelector(selector);
    window.setTimeout(function () {
        $(c).animate({scrollTop: 0}, 500);
    }, 600);
}
export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const superws = "https://frisian.eu/TEST/superlemmalist-ws/graphql"
const superquery = `query super($lemma: String! $lang: ISO639_2!) {
findsuperlemmas (lemma: $lemma language: $lang) { superlemmas { code pos lemmas { lemma languagecode } } } }
`
const langapi = "https://frisian.eu/languageapi"
const langquery = `query frysk($mfry: String! $lang: LangType!) {
        lemmasearch (searchterm: $mfry lang: $lang) {lemmas {translations {form}}}
    }
`
export function distinctJoin(val) {
    return [...new Set(val)].sort().join('|')
}
export function nlToFry(state) {
    request(langapi,langquery,{mfry: state.value, lang: "nl"}).then((data) => {
            state.value = distinctJoin(data.lemmasearch.lemmas.map(a => a.translations).flat().map(a => a.form))
        }
    );
}
export function findSuper(lemma, lang) {
    return request(superws,superquery,{lemma: lemma, lang: lang}).then((data) => {
            return data;
        }
    );
}
