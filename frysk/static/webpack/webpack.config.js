const path = require('path');

module.exports = {
  entry: './src/index.js',
  // mode: "development",
  output: {
    filename: 'langapi.js',
    path: path.resolve(__dirname, 'dist'),
    library: {
      name: 'langapi',
      type: 'var',
    }
  },
};
